#!/bin/bash
############################################################
# network_init.sh
# Author : Ronan OLIVE
# Purpose : Set static IP and hostname of host
# Vars :
#   - ip : IP address to set
#   - mask : netmask to set
#   - gw : gateway for the network
#   - int : network interface to configure
#   - hname : hostname to set
############################################################
############################################################
# Help                                                     #
############################################################
Help()
{
    # Display Help
    echo "This script sets the IP address and hostname of its host"
    echo
    echo "Syntax: ./network_init.sh [-i <IP_ADDRESS>] [-m <NETMASK>] [-g <GATEWAY>] [-n <INTERFACE>] [-o <HOSTNAME>] [-h]"
    echo "options:"
    echo "-g : Specify network gateway IP"
    echo "-h : Print this help"
    echo "-i : Specify IP address to se"
    echo "-m : Specify NetMask to of the network"
    echo "-n : Specify the iNterface to configure"
    echo "-o : Specify the hostname to set"
    echo
}
############################################################

############################################################
# Options handling                                         #
############################################################
while getopts ":g:h:i:m:n:o:" option; do
   case $option in
    g) # set Gateway var
        gw=$OPTARG;;
    h) # display Help
        Help
        exit;;
    i) # display Help
        ip=$OPTARG;;
    m) # display Help
        mask=$OPTARG;;
    n) # display Help
        int=$OPTARG;;
    o) # display Help
        hname=$OPTARG;;
    \?) # Invalid option
        echo "Error: Invalid option -$OPTARG."
        echo
        Help
        exit;;
   esac
done
############################################################

############################################################
# Main program                                             #
############################################################

echo "############################################################"
echo "#                  Network configuration                   #"
echo "############################################################"
echo

if [[ ! -n $ip ]] || ! [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
    read -p "IP address : " ip
    echo
    while [[ -z {$ip+x} ]] || ! [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; do
        echo "Invalid Input. Please type an IP address."
        read -p "IP address : " ip
        echo
    done
fi

if [[ ! -n $mask ]] || ! [[ $mask =~ (^[0-9]{1,2}$)|(^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$) ]]; then
    read -p "Network mask : " mask
    echo
    while [[ -z {$mask+x} ]] || ! [[ $mask =~ (^[0-9]{1,2}$)|(^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$) ]]; do
        echo "Invalid Input. Please type an either a CIDR or VLSM mask."
        echo "Mask : $mask"
        read -p "Network mask : " mask
        echo
    done
fi

if [[ ! -n $gw ]] || ! [[ $gw =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
    read -p "Gateway IP address : " gw
    echo
    while [[ -z {$gw+x} ]] || ! [[ $gw =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; do
        echo "Invalid Input. Please type an IP address."
        read -p "Gateway IP address : " gw
        echo
    done
fi

readarray -t int_array <<< "$(ip link show | awk -F " " 'NR % 2 == 1  { gsub(":","",$2);print $2 }')"

if [[ ! -n $int ]] || ! [[ ${int_array[*]} =~ $int ]]; then
    echo "Available network interfaces :"
    for i in "${int_array[@]}";do
        echo "  - $i"
    done
    read -p "Network interface : " int
    echo
    while [[ -z {$int+x} ]] || ! [[ ${int_array[*]} =~ $int ]]; do
        echo "Invalid Input. Choose an available interface."
        read -p "Network interface : " int
        echo
    done
fi

if [[ ! -n $hname ]]; then
    read -p "Do you want to configure the hostname ? [Y/n] : " ask
    if [[ ! -n $ask ]] || [[ $ask =~ ^[y|Y](es)? ]]; then
        read -p "Hostname : " hname
        while [[ -z {$hname+x} ]]; do
            echo "Invalid Input. Enter a non-null hostname."
            read -p "Hostname : " hname
            echo
        done
        echo
    fi
fi

echo "[INFO] - Configuring Static IP"
if [[ $mask =~ ^[0-9]{1,2}$ ]]; then
cat >> /etc/network/interfaces << EOF
auto $int
iface $int inet static
    address $ip/$mask
    gateway $gw
EOF
    if [[ ${?} > 0 ]]; then
        echo "[ERROR] - Writing to /etc/network/interfaces."
        exit 1
    fi
else
cat >> /etc/network/interfaces << EOF
auto $int
iface $int inet static
    address $ip
    netmask $mask
    gateway $gw
EOF
    if [[ ${?} > 0 ]]; then
    echo "[ERROR] - Writing to /etc/network/interfaces."
    exit 1
    fi
fi

echo "[INFO] - Restarting Networking service"
systemctl restart networking > /dev/null 2>&1
if [[ ${?} > 0 ]]; then
    echo "[ERROR] - Error restarting Networking service."
    reb="yes"
fi


if [[ -n $hname ]]; then
    echo "[INFO] - Setting hostname to $hname"
    hostnamectl set-hostname "$hname" >/dev/null 2>&1
    if [[ ${?} > 0 ]]; then
        echo "[ERROR] - Error setting hostname."
    fi
    cat >> /etc/hosts << EOF
127.0.1.1       $hname
EOF
fi

if [[ $reb = "yes" ]]; then
    echo "A reboot is needed to take changes into account"
fi

read -p "press [ENTER] to continue"
############################################################
